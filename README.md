# TUTORIAL REPO MSP430 EMULATOR #

Repository just for students studying of C++ programming, microprocessor emulation architecture and git working.

### MSP430 ###

[Good operation codes descriptions](https://en.wikipedia.org/wiki/TI_MSP430)

[Simple Python MSP430 emulator](https://bitbucket.org/olapsema/msp430emu)

### C++ ###

[Software design pattern](https://en.wikipedia.org/wiki/Software_design_pattern)

### Git links ###
[Git Book](https://git-scm.com/book/ru/v1)

[GitHub Flow](https://guides.github.com/introduction/flow/)

[Git Flow](http://danielkummer.github.io/git-flow-cheatsheet/index.ru_RU.html)