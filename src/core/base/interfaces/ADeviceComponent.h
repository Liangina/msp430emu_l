//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_DEVICE_COMPONENT_H
#define MSP430EMU_DEVICE_COMPONENT_H

namespace vmc {
    class ADevice;
    typedef std::shared_ptr<ADevice> PDevice;

    class ADeviceComponent {
    protected:
        PDevice dev;
    public:
        void device(PDevice vdev) {
            dev = vdev;
        }
    };

    typedef std::shared_ptr<ADeviceComponent> PDeviceComponent;
}

#endif //MSP430EMU_DEVICE_COMPONENT_H
