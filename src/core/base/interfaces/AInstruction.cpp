//
// Created by Alexei Gladkikh on 16/11/16.
//

#include "AInstruction.h"
#include "AOperand.h"

namespace vmc {
    std::string AInstruction::stringify() {
        std::stringstream ss;
        auto sea = utils::sformat("[%08X]", ea);
        ss << sea << " " << mnem;
        if (op1->type != AOperand::VOID1)
            ss << " " << op1->stringify();
        if (op2->type != AOperand::VOID1)
            ss << ", " << op2->stringify();
        return ss.str();
    }
}