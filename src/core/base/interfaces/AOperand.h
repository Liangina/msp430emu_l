//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_AOPERAND_H
#define MSP430EMU_AOPERAND_H

#include <cstdint>
#include <memory>
#include "../../../utils/utils.h"
#include "ADevice.h"

namespace vmc {
//    class ADevice;
//    typedef std::shared_ptr<ADevice> PDevice;

    class AOperand {
    public:
        enum Type { VOID1, REGISTER, MEMORY, IMMEDIATE, DISPLACEMENT, PHRASE };
        enum Size { UNDEF, BYTE, WORD, DWORD, QWORD };

        const Type type = VOID1;
        const Size size = UNDEF;

        virtual uint32_t value(PDevice dev) const = 0;
        virtual void value(PDevice dev, uint32_t data) const = 0;

        uint32_t bit(PDevice dev, int bitno) const {
            return utils::bit(value(dev), bitno);
        }

        void bit(PDevice dev, int bitno, uint32_t bit) const {
            auto data = value(dev);
            utils::bit(data, bitno, bit);
            value(dev, data);
        }

        virtual std::string stringify() const = 0;
    protected:
        AOperand(Type vtype, Size vsize) : type(vtype), size(vsize) { };
    };

    typedef std::shared_ptr<AOperand> PAOperand;
}

#endif //MSP430EMU_AOPERAND_H
