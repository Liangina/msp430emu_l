//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_DUMMY_DISPLACEMENT_H
#define MSP430EMU_DUMMY_DISPLACEMENT_H

#include "../../base/interfaces/AOperand.h"

namespace dummy {
    class Displacement : public vmc::AOperand {

    };
}

#endif //MSP430EMU_DISPLACEMENT_H
