//
// Created by Ignaty Deltsov on 01/11/16.
//

#ifndef MSP430EMU_MSP430_INTERUPTCONTROLLER_H
#define MSP430EMU_MSP430_INTERUPTCONTROLLER_H

#include <cstdint>
#include "../../base/interfaces/AInterruptController.h"
#include "../operands/Register.h"
#include "CPU.h"
#include <list>

namespace msp430 {
    class InterruptController : public vmc::AInterruptController {
    private:

        vmc::PAPeripheral lookupPendingIR(){
            vmc::PAPeripheral result = nullptr;
            auto GIE = Register::SR->bit(dev, STATUS_GIE);
            if (!GIE) return 0;
            uint32_t tempPrority = 0;

            for (auto it:dev->periphs)
            {
                if (it->interuptEnableBit && it->interuptPendingFlag)
                {
                    if (it->priority > tempPrority)
                    {
                        tempPrority = it->priority;
                        result = it;
                    }
                    it->interuptPendingFlag = false;
                }
            }
            return result;
        }

    public:

        bool processInterrupt() override {
            auto pendingPeriph = lookupPendingIR();
            if (!pendingPeriph)
            {
                return true;
            }

            auto sp = Register::SP->value(dev);
            auto pc = Register::PC->value(dev);
            auto sr = Register::SR->value(dev);
            sp -= 2;
            dev->memory->storeWord(sp, pc);
            sp -= 1;
            dev->memory->storeByte(sp, sr);
            Register::SP->value(dev, sp);
            Register::SR->bit(dev, STATUS_C, 0);
            Register::SR->bit(dev, STATUS_Z, 0);
            Register::SR->bit(dev, STATUS_N, 0);
            Register::SR->bit(dev, STATUS_V, 0);
            Register::SR->bit(dev, STATUS_GIE, 0);

            auto vectorAddrIR = pendingPeriph->vectorAdressIR;
            auto addrHandlerIr = dev->memory->loadWord(vectorAddrIR);
            Register::PC->value(dev, addrHandlerIr);

            return true;
        }
    };
}

#endif //MSP430EMU_MSP430_INTERUPTCONTROLLER_H
