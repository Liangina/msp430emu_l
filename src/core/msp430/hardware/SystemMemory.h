//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_MSP430_SYSTEMMEMORY_H
#define MSP430EMU_MSP430_SYSTEMMEMORY_H

#include <cstdint>
#include <vector>
#include "../../base/interfaces/AMemory.h"
#include "../../base/exceptions/exceptions.h"
#include "../../base/interfaces/ADevice.h"

namespace msp430 {
    class SystemMemory : public vmc::AMemory {
    private:
        std::vector<uint8_t> _data;
        uint32_t read_reg(uint32_t vAddr, bool &isOk) const {
            isOk = false;
            if (vAddr < 0x200) {
                for (auto periph : dev->periphs) {
                    for (auto reg : periph->registers) {
                        if (reg->isAccessed(vAddr, 1)) {
                            isOk = true;
                            return reg->onRead(vAddr, 1);
                        }
                    }
                }
            }
            return 0;
        }

        void write_reg(uint32_t vAddr, uint32_t data, bool &isOk) const {
            isOk = false;
            if (vAddr < 0x200) {
                for (auto periph : dev->periphs) {
                    for (auto reg : periph->registers) {
                        if (reg->isAccessed(vAddr, 1)) {
                            isOk = true;
                            reg->onWrite(vAddr, data, 1);
                        }
                    }
                }
            }
        }
    public:
        SystemMemory(int ramSize, int romSize) {
            _data = std::vector<uint8_t>(0x10000);
        }

        void blockWrite(uint32_t vAddr, std::vector<uint8_t> bytes) override {
            for (int k = 0; k < bytes.size(); k++)
                _data[vAddr + k] = bytes[k];
        }

        std::vector<uint8_t> blockRead(uint32_t vAddr, int size) const override {
            auto result = std::vector<uint8_t>((unsigned long) size);
            for (int k = 0; k < size; k++) {
                result[k] = _data[vAddr + k];
            }
            return result;
        }

        uint32_t loadByte(uint32_t vAddr) const override {
            bool isReg = false;
            uint32_t result = read_reg(vAddr, isReg);
            if (isReg) {
                return result;
            } else {
                return (uint32_t) (_data[vAddr]);
            }
        }

        uint32_t loadWord(uint32_t vAddr) const override {
            bool isReg = false;
            uint32_t result = read_reg(vAddr, isReg);
            if (isReg) {
                return result;
            } else {
                return (uint32_t) (_data[vAddr] || _data[vAddr+1]<<8);
            }
        }

        uint32_t loadDword(uint32_t vAddr) const override {
            throw exc::UnimplementedException();
        }

        void storeByte(uint32_t vAddr, uint32_t data) override {
            bool isReg = false;
            write_reg(vAddr, data, isReg);
            if (!isReg) {
                _data[vAddr] = (uint8_t) data;
            }
        }

        void storeWord(uint32_t vAddr, uint32_t data) override {
            bool isReg = false;
            write_reg(vAddr, data, isReg);
            if (!isReg) {
                _data[vAddr] = (uint8_t) data;
                _data[vAddr+1] = (uint8_t) (data >> 8);
            }
        }

        void storeDword(uint32_t vAddr, uint32_t data) override {
            throw exc::UnimplementedException();
        }
    };
}

#endif //MSP430EMU_MSP430_SYSTEMMEMORY_H
