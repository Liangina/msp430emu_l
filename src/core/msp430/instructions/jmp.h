//
// Created by Alexei Gladkikh on 05/10/16.
//

#ifndef MSP430EMU_JMP_H
#define MSP430EMU_JMP_H

#include "../../base/interfaces/AInstruction.h"
#include "../operands/Void.h"
#include "../operands/Register.h"

namespace msp430 {
    class Jmp : public vmc::AInstruction {
    public:
        Jmp(const vmc::PAOperand vop1) :
                AInstruction("jmp", vop1, std::make_shared<Void>(), 2) { }

        bool execute(vmc::PDevice dev) override {
            auto pc = Register::PC->value(dev);
            pc += op1->value(dev) + size;
            Register::PC->value(dev, pc);
            return true;
        }
    };
}

#endif //MSP430EMU_JMP_H
