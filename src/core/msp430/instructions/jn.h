//
// Created by Alexei Gladkikh on 05/10/16.
//

#ifndef MSP430EMU_JN_H
#define MSP430EMU_JN_H

#include "../../base/interfaces/AInstruction.h"
#include "../operands/Void.h"
#include "../operands/Register.h"
#include "../hardware/CPU.h"

namespace msp430 {
    class Jn : public vmc::AInstruction {
    public:
        Jn(const vmc::PAOperand vop1) :
                AInstruction("jn", vop1, std::make_shared<Void>(), 2) { }

        bool execute(vmc::PDevice dev) override {
            auto n = Register::SR->bit(dev, STATUS_N);
            if (n==1) {
                auto pc = Register::PC->value(dev);
                pc += op1->value(dev) + size;
                Register::PC->value(dev, pc);
            }
            return true;
        }
    };
}

#endif //MSP430EMU_JN_H
