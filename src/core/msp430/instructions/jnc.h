//
// Created by Alexei Gladkikh on 05/10/16.
//

#ifndef MSP430EMU_JNC_H
#define MSP430EMU_JNC_H

#include "../../base/interfaces/AInstruction.h"
#include "../operands/Void.h"
#include "../operands/Register.h"
#include "../hardware/CPU.h"

namespace msp430 {
    class Jnc : public vmc::AInstruction {
    public:
        Jnc(const vmc::PAOperand vop1) : AInstruction("jnc", vop1, std::make_shared<Void>(), 2) { }

        bool execute(vmc::PDevice dev) override {
            auto c = Register::SR->bit(dev, STATUS_C);
            if (c==0) {
                auto pc = Register::PC->value(dev);
                pc += op1->value(dev) + size;
                Register::PC->value(dev, pc);
            }
            return true;
        }
    };
}

#endif //MSP430EMU_JC_H
