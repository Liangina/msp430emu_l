//
// Created by Alexei Gladkikh on 05/10/16.
//

#ifndef MSP430EMU_MOV_H
#define MSP430EMU_MOV_H

#include "../../base/interfaces/AInstruction.h"

namespace msp430 {
    class Mov : public vmc::AInstruction {
    public:
        Mov(const vmc::PAOperand vop1, const vmc::PAOperand vop2, const int size) :
                AInstruction("mov", vop1, vop2, size) { }

        bool execute(vmc::PDevice dev) override {
            auto src = this->op1->value(dev);
            this->op2->value(dev, src);
            return true;
        }
    };
}

#endif //MSP430EMU_MOV_H
