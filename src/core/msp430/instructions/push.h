//
// Created by Alexei Gladkikh on 05/10/16.
//

#ifndef MSP430EMU_PUSH_H
#define MSP430EMU_PUSH_H


#include "../../base/interfaces/ADevice.h"
#include "../operands/Void.h"
#include "../operands/Register.h"

namespace msp430 {
    class Push : public vmc::AInstruction {
    public:
        Push(const vmc::PAOperand vop1) :
                AInstruction("push", vop1, std::make_shared<Void>(), size) { }

        bool execute(vmc::PDevice dev) override {
            auto sp = Register::SP->value(dev);
            // TODO: Check if byte possible
            sp -= 2;
            auto src = op1->value(dev);
            dev->memory->storeWord(sp, src);
            Register::SP->value(dev, sp);
            return true;
        }
    };
}

#endif //MSP430EMU_PUSH_H
