//
// Created by ALG on 01.11.2016.
//

#ifndef MSP430EMU_RETI_H
#define MSP430EMU_RETI_H
#include "../../base/interfaces/ADevice.h"
#include "../operands/Void.h"
#include "../operands/Register.h"

namespace msp430 {
    class Reti : public vmc::AInstruction {
    public:
        Reti(const vmc::PAOperand vop1) :
                AInstruction("reti", vop1, std::make_shared<Void>(), 2) { }

        bool execute(vmc::PDevice dev) override {

            return true;
        }
    };
}
#endif //MSP430EMU_RETI_H
