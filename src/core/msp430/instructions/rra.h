//
// Created by ALG on 01.11.2016.
//

#ifndef MSP430EMU_RRA_H
#define MSP430EMU_RRA_H
#include "../../base/interfaces/ADevice.h"
#include "../operands/Void.h"
#include "../operands/Register.h"
#include "../hardware/CPU.h"

namespace msp430 {
    class Rra : public vmc::AInstruction {
    public:
        Rra(const vmc::PAOperand vop1) :
                AInstruction("rra", vop1, std::make_shared<Void>(), size) { }

        bool execute(vmc::PDevice dev) override {
            auto lsb = op1->bit(dev, 0);
            if (op1->size==vmc::AOperand::BYTE){
                auto msb = op1->bit(dev, 7);
                auto dst = ((op1->value(dev))>>1)&0x3F;
                dst|=msb<<7;
                this->op2->value(dev, dst);
                if (msb)
                    Register::SR->bit(dev, STATUS_N, 1);
                else Register::SR->bit(dev, STATUS_N, 0);
            }
            if (op1->size==vmc::AOperand::WORD){
                auto msb = op1->bit(dev, 15);
                auto dst = ((op1->value(dev))>>1)&0x3FFF;
                dst|=msb<<15;
                this->op2->value(dev, dst);
                if (msb)
                    Register::SR->bit(dev, STATUS_N, 1);
                else Register::SR->bit(dev, STATUS_N, 0);
                 }
            Register::SR->bit(dev, STATUS_C, lsb);
            Register::SR->bit(dev, STATUS_V, 0);
            return true;
        }
    };
}

#endif //MSP430EMU_RRA_H
