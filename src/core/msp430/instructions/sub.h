//
// Created by Alexei Gladkikh on 05/10/16.
//

#ifndef MSP430EMU_SUB_H
#define MSP430EMU_SUB_H

#include "../../base/interfaces/AInstruction.h"

namespace msp430 {
    class Sub : public vmc::AInstruction {
    public:
        Sub(const vmc::PAOperand vop1, const vmc::PAOperand vop2, const int size) :
                AInstruction("sub", vop1, vop2, size) { }

        bool execute(vmc::PDevice dev) override {
            auto src = this->op1->value(dev);
            auto dst = this->op2->value(dev);
            auto res = dst - src;
            this->op2->value(dev, res);
            if (op1->size == vmc::AOperand::BYTE) { //set overflow flag
                if (((~dst&src&res) >> 7)||((dst&~src&~res) >> 7)) {
                    Register::SR->bit(dev, STATUS_V, 1);
                } else Register::SR->bit(dev, STATUS_V, 0);
                if (((src & ~dst & res) >> 7) || ((~src & dst & res) >> 7)) {
                    Register::SR->bit(dev, STATUS_C, 1);
                } else Register::SR->bit(dev, STATUS_C, 0);
                if (res >> 7) {
                    Register::SR->bit(dev, STATUS_N, 1);
                } else Register::SR->bit(dev, STATUS_N, 0);
                if (res==0x00)
                    Register::SR->bit(dev, STATUS_Z, 1);
            }
            if (op1->size == vmc::AOperand::WORD) { //set overflow flag
                if (((~dst&src&res) >> 15)||((dst&~src&~res) >> 15)) {
                    Register::SR->bit(dev, STATUS_V, 1);
                } else Register::SR->bit(dev, STATUS_V, 0);
                if (((src & ~dst & res) >> 15) || ((~src & dst & res) >> 15)) {
                    Register::SR->bit(dev, STATUS_C, 1);
                } else Register::SR->bit(dev, STATUS_C, 0);
                if (res >> 15) {
                    Register::SR->bit(dev, STATUS_N, 1);
                } else Register::SR->bit(dev, STATUS_N, 0);
                if (res==0x0000)
                    Register::SR->bit(dev, STATUS_Z, 1);
            }

            return true;
        }
    };
}

#endif //MSP430EMU_ADD_H
