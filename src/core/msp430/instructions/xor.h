//
// Created by Alexei Gladkikh on 05/10/16.
//

#ifndef MSP430EMU_XOR_H
#define MSP430EMU_XOR_H

#include "../../base/interfaces/AInstruction.h"

namespace msp430 {
    class Xor : public vmc::AInstruction {
    public:
        Xor(const vmc::PAOperand vop1, const vmc::PAOperand vop2, const int size) :
                AInstruction("xor", vop1, vop2, size) { }

        bool execute(vmc::PDevice dev) override {
            auto src = this->op1->value(dev);
            auto dst = this->op2->value(dev);
            auto src_sign_b = op1->bit(dev, 7);
            auto src_sign_w = op1->bit(dev, 15);
            auto dst_sign_b = op2->bit(dev, 7);
            auto dst_sign_w = op2->bit(dev, 15);
            auto res = src ^ dst;
            this->op2->value(dev, res);
            if (op1->size==vmc::AOperand::BYTE)
            {
                auto msb = op2->bit(dev,7);
                if (msb)
                    Register::SR->bit(dev, STATUS_N, 1);
                else Register::SR->bit(dev, STATUS_N, 0);
                if (src_sign_b & dst_sign_b)
                    Register::SR->bit(dev, STATUS_V, 1);
                else
                Register::SR->bit(dev, STATUS_V, 0);
                if (res==0x00)
                {
                    Register::SR->bit(dev, STATUS_Z, 1);
                    Register::SR->bit(dev, STATUS_C, 0);
                }
                else
                {
                    Register::SR->bit(dev, STATUS_Z, 0);
                    Register::SR->bit(dev, STATUS_C, 1);
                }
                }

            if (op1->size==vmc::AOperand::WORD)
            {
                auto msb = op2->bit(dev,15);
                if (msb)
                    Register::SR->bit(dev, STATUS_N, 1);
                else Register::SR->bit(dev, STATUS_N, 0);
                if (src_sign_w & dst_sign_w)
                    Register::SR->bit(dev, STATUS_V, 1);
                else
                    Register::SR->bit(dev, STATUS_V, 0);
                if (res==0x0000)
                {
                    Register::SR->bit(dev, STATUS_Z, 1);
                    Register::SR->bit(dev, STATUS_C, 0);
                }
                else
                {
                    Register::SR->bit(dev, STATUS_Z, 0);
                    Register::SR->bit(dev, STATUS_C, 1);
                }
            }

            return true;
        }
    };
}

#endif //MSP430EMU_XOR_H
