//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_MSP430_DISPLACEMENT_H
#define MSP430EMU_MSP430_DISPLACEMENT_H

#include "../../base/interfaces/AOperand.h"

namespace msp430 {
    class Displacement : public vmc::AOperand {
    public:
        const int rid;
        const uint32_t offset;
        const bool postinc;

        Displacement(int vrid, uint32_t voffset, Size vsize, bool vpostinc = false) :
                rid(vrid), offset(voffset), postinc(vpostinc), AOperand(DISPLACEMENT, vsize) {}

        // TODO: Add 16-bit mask on value
        uint32_t value(vmc::PDevice dev) const override {
            uint32_t address = dev->cpu->reg(rid);
            if (postinc) {
                dev->cpu->reg(rid, address + 2);
            }
            address += offset;
            return dev->memory->loadWord(address);
        }

        // TODO: Add 16-bit mask on value
        void value(vmc::PDevice dev, uint32_t data) const override {
            uint32_t address = dev->cpu->reg(rid);
            //Is postincrement necessary??!
            address += offset;
            data &= 0xFFFF;
            dev->memory->storeWord(address, data);
            return;
        }

        std::string stringify() const override {
            return utils::sformat("[R%d+0x%04X]", rid, offset);
        }
    };
}

#endif //MSP430EMU_MSP430_DISPLACEMENT_H
