//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_MSP430_MEMORY_H
#define MSP430EMU_MSP430_MEMORY_H

#include "../../base/interfaces/AOperand.h"
#include "../../base/exceptions/exceptions.h"

namespace msp430 {
    class Memory : public vmc::AOperand {
    public:
        uint32_t address;

        Memory(uint32_t vaddress, Size vsize) : address(vaddress), AOperand(IMMEDIATE, vsize) {}

        uint32_t value(vmc::PDevice dev) const override {
            if (size == BYTE) {
                return dev->memory->loadByte(address);
            } else if (size == WORD) {
                return dev->memory->loadWord(address);
            } else {
                throw new exc::UnsupportOperationException();
            }
        }

        // TODO: Add 16-bit mask on value
        void value(vmc::PDevice dev, uint32_t data) const override {
            if (size == BYTE) {
                return dev->memory->storeByte(address, data);
            } else if (size == WORD) {
                return dev->memory->storeWord(address, data);
            } else {
                throw new exc::UnsupportOperationException();
            }
        }

        std::string stringify() const override {
            return utils::sformat("%04X", address);
        }
    };
}

#endif //MSP430EMU_MSP430_MEMORY_H
