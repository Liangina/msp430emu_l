#include "Register.h"

namespace msp430 {
    const std::shared_ptr<Register> Register::PC = std::make_shared<msp430::Register>(0, vmc::AOperand::WORD);
    const std::shared_ptr<Register> Register::SP = std::make_shared<msp430::Register>(1, vmc::AOperand::WORD);
    const std::shared_ptr<Register> Register::SR = std::make_shared<msp430::Register>(2, vmc::AOperand::WORD);
};