#include "thirdparty/easylogging/easylogging++.h"

INITIALIZE_EASYLOGGINGPP

//#include "gdbstub/Server.h"
#include "core/msp430/Device.h"

#define log el::Loggers::getLogger("main")

int main(int argc, char* argv[]) {
    log->info("MSP430 Virtual machine started");
    auto target = std::make_shared<msp430::Device>(0x8000, 0x4000);
    // auto server = gdb::Server(27026, target);
    // server.doListen();

    target->execute();

    return 0;
}
