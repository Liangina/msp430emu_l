import idaapi
from idc import *
from idautils import Functions
import json
from binascii import hexlify

def applySign(signs):
    total = 0
    info = dict()
    for ea in Functions(0, 0x10000):
        func = idaapi.get_func(ea)
        size = func.endEA - func.startEA
        data = idaapi.get_many_bytes(func.startEA, size)
        sign = hexlify(data)
        print("%08X sign = %s" % (func.startEA, sign))
        if sign in signs:
            name = signs[sign]
            MakeName(func.startEA, "x_" + name)
            total += 1
            print("Apply to %08X name %s" % (func.startEA, name))
    print("Done, total function distinguish = %d" % total)

    
def main():
    with open("signs.json", "rt") as ifile:
        signs = json.load(ifile)
    signs = {str(k): str(v) for k, v in signs.items()}
    applySign(signs)

    
if __name__ == "__main__":
    main()