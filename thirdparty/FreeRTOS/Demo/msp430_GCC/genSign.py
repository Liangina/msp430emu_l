import idaapi
from idc import *
from idautils import Functions
import json
from binascii import hexlify

def genSign():
    result = dict()
    collisions = set()
    for ea in Functions(0, 0x10000):
        func = idaapi.get_func(ea)
        size = func.endEA - func.startEA
        data = idaapi.get_many_bytes(func.startEA, size)
        sign = hexlify(data)
        if sign not in result:
            result[sign] = GetFunctionName(func.startEA)
        else:
            collisions.add(sign)
    for collision in collisions:
        del result[collision]
    print("Done, total function = %d" % len(result))
    return result

def main():
    signs = genSign()
    with open("signs.json", "wt") as ofile:
        ofile.write(json.dumps(signs, indent=4))
        
if __name__ == "__main__":
    main()